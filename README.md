# designComponent

## 脚手架启动打包命令
```
  npm run zip 最后会将打包后的对应文件生成component.zip压缩包在dist目录下
  目录结构为
  dist
    src [组件代码源文件]
    package [npm包]
    lib [npm包]
    components [打包后的可执行源文件]
    manifest.json [自定义文件]
    package.json [依赖文件以及包名]
```

## Project setup
```
pnpm install
```

### Compiles and hot-reloads for development
```
pnpm run serve
```

### Compiles and minifies for production
```
pnpm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
