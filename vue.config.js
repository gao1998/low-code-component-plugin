const { defineConfig } = require('@vue/cli-service')
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = defineConfig({
  css: {
    extract: false, // 关闭 CSS 提取，以便样式可以注入到 lib 中
  },
  transpileDependencies: true,
  configureWebpack: {
    plugins: [
      new NodePolyfillPlugin()
    ],
    output: {
      filename: '[name].js',
      libraryTarget: 'umd'
    }
  },
  chainWebpack: config => {
    config.module
      .rule('js')
      .test(/\.js$/)
      .use('babel-loader')
      .loader('babel-loader')
      .end();
    
    if (process.env.NODE_ENV === "production"){
      config.plugin("copy").use(CopyWebpackPlugin, [{patterns: [
        { from: "src/**/*"},
        { from: "package/**/*"},
        // { from: "lib" },
        { from: "package.json" },
        { from: "manifest.json" }
      ]}]);
    }
  }
})
