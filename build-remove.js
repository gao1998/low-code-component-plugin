/**
 * 删除打包目录结构下多余的js文件
 */

const fs = require('fs');
const path = require('path');

const directoryPath = 'dist'; // 目标目录路径

// 读取目录下的所有文件
fs.readdir(directoryPath, (err, files) => {
  if (err) {
    console.error('Error reading directory:', err);
    return;
  }

  // 筛选出需要删除的文件
  const filesToDelete = files.filter(file => file.endsWith('.js') || file.endsWith('.js.map'));

  // 删除文件
  filesToDelete.forEach(file => {
    const filePath = path.join(directoryPath, file);
    fs.unlink(filePath, err => {
      if (err) {
        console.error(`Error deleting file ${file}:`, err);
      } else {
        console.log(`${file} deleted successfully`);
      }
    });
  });
});