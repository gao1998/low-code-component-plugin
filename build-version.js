//npm run build打包前执行此段代码
let fs = require('fs')

//返回package的json数据
function getPackageJson() {
  let data = fs.readFileSync('./package.json') //fs读取文件
  return JSON.parse(data) //转换为json对象
}

let packageData = getPackageJson() //获取package的json
let arr = packageData.version.split('.') //切割后的版本号数组
let finallyNum = arr.pop();
finallyNum = parseInt(finallyNum) + 1
packageData.version = arr.join('.') +  '.' + finallyNum //转换为以"."分割的字符串
//用packageData覆盖package.json内容
fs.writeFile('./package.json', JSON.stringify(packageData, null, '\t'), err => {
  console.log("版本号自动累加成功")
})