const fs = require('fs');
const archiver = require('archiver');

const output = fs.createWriteStream('dist/component.zip');
const archive = archiver('zip', {
  zlib: { level: 9 } // 设置压缩级别
});

output.on('close', () => {
  console.log(archive.pointer() + ' total bytes');
  console.log('压缩包创建成功');
});

archive.on('error', err => {
  throw err;
});

archive.pipe(output);

// 打包忽略自己防止将自己也打包进去,会出现多余空包的问题
// 将 dist 目录下的所有文件添加到压缩包
// archive.directory('dist/', false);

archive.directory('dist/components', 'components');
archive.directory('dist/package', 'package');
archive.directory('dist/src', 'src');
archive.file('dist/manifest.json', { name: 'manifest.json' });
archive.file('dist/package.json', { name: 'package.json' });

// 将 lib 目录下的文件添加到压缩包
archive.directory('lib', 'lib');

archive.finalize();