/**
 * 该方法为移除上一次打包的dist文件夹
 */

const fs = require('fs');

if (fs.existsSync('dist')) {
  fs.rmdirSync('dist', { recursive: true });
}