import path from "path";

/**
 * 批量引入组件
 */
const importSystemComponents = () => {
  // 获取components下导出的所有组件
  const importAll = require.context('./components', true, /\.(js)$/);

  const modules = {};

  importAll.keys().forEach(key => {
    const name = path.basename(key, '.js');
    modules[name] = importAll(key).default || importAll(key)
  })

  return modules
}

// 组件列表
const components = importSystemComponents()

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，那么所有的组件都会被注册
const install = (Vue) => {
  // 判断是否安装
  if(!install.installed){
    // 遍历注册组件
    Object.keys(components).forEach(v=>{
      Vue.component(components[v].name, components[v]);
    })
  }
}

// 判断是否是直接引入组件
if(typeof window !== 'undefined' && window.Vue){
  install(window.Vue)
}

export default {
  install,
  ...components
}
