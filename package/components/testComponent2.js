import testComponent2 from '@/components/testComponent2/runtime/testComponent2.vue';

testComponent2.install = (Vue) => {
  Vue.component(testComponent2.name, testComponent2)
}

export default testComponent2