import testComponent from '@/components/testComponent/runtime/testComponent.vue';

testComponent.install = (Vue) => {
  Vue.component(testComponent.name, testComponent)
}

export default testComponent